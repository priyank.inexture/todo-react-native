const today = new Date();
let tomorrow = new Date(today);
let upcoming = new Date(today);
tomorrow.setDate(today.getDate() + 1);
upcoming.setDate(today.getDate() + 2);
export const staticTaskList = [
  {
    id: Math.random().toString(),
    text: "Plan the family trip to Norway",
    time: today,
    status: false,
  },
  {
    id: Math.random().toString(),
    text: "Plan David's birthday event",
    time: today,
    status: true,
  },
  {
    id: Math.random().toString(),
    text: "Groceries for dinner",
    time: today,
    status: false,
  },
  {
    id: Math.random().toString(),
    text: "Send the presentation to Jeff",
    time: tomorrow,
    status: false,
  },
  {
    id: Math.random().toString(),
    text: "Take the jacket to dry cleaning",
    time: tomorrow,
    status: false,
  },
  {
    id: Math.random().toString(),
    text: "Fix dad's tablet",
    time: tomorrow,
    status: false,
  },
  {
    id: Math.random().toString(),
    text: "Talk with Steve about the trip",
    time: upcoming,
    status: false,
  },
  {
    id: Math.random().toString(),
    text: "Plan the family trip to India",
    time: upcoming,
    status: false,
  },
];
