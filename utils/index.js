import { staticTaskList } from "./taskList";
export const getDateType = (timeProp) => {
  let time = timeProp.getTime();
  let today = new Date();
  today.setHours(0, 0, 0, 0);
  let tomorrow = new Date(today);
  let upcoming = new Date(today);
  tomorrow.setDate(today.getDate() + 1);
  upcoming.setDate(today.getDate() + 2);
  if (time < today.getTime()) {
    //yesterday
    return -1;
  } else if (time >= today.getTime() && time <= tomorrow.getTime()) {
    //today
    return 0;
  } else if (time >= today.getTime() && time <= upcoming.getTime()) {
    //tomorrow
    return 1;
  }
  //upcomming
  return 2;
};
export const taskList = staticTaskList;
