import React from 'react'
import { render,fireEvent} from '@testing-library/react-native';
import Header from '../components/Header'
describe("Header",()=>{
    test('Initial renders correctly',()=>{
        const {getByText} = render(<Header title="Home"/>);
        expect(getByText("Home")).toBeTruthy();                
    })
    test('Renders without prop',()=>{
        const {getByText} = render(<Header />);
        expect(getByText("Demo-app")).toBeTruthy();                
    })
});
