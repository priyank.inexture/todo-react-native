import React from 'react'
import { render,fireEvent} from '@testing-library/react-native';
import EmptyTask from '../components/EmptyTask'
describe("EmptyTask",()=>{
    test('Initial render',()=>{
        const {getByText} = render(<EmptyTask/>);
        expect(getByText("No Task")).toBeTruthy();                
    })
});
