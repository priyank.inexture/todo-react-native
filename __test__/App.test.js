import React from 'react';
// import renderer from 'react-test-renderer';
import App from '../App'
import EmptyTask from '../components/EmptyTask';
import NewTodo from '../components/NewTodo'
import { render, fireEvent, waitFor} from '@testing-library/react-native';
describe("App test",()=>{
    it("should initial App render",()=>{
        const  {getByTestId} = render(<App/>)
        const container = getByTestId('todo-container');
        const addButton = getByTestId('add-button');
        expect(container.children[0].type).toBe(EmptyTask);    
        expect(addButton).toBeTruthy();
    })
    it("should have Add button",()=>{
        const  {getByTestId} = render(<App/>)
        const addButton = getByTestId('add-button');
        expect(addButton).toBeTruthy();
        // fireEvent.press(addButton);
    })
    it("should add a Task",()=>{
        const  {getByTestId,getByPlaceholderText,getByText} = render(<App/>)
        const addButton = getByTestId('add-button');
        expect(addButton).toBeTruthy();
        fireEvent.press(addButton);
        const textInput = getByPlaceholderText('Enter Task');
        const createTask = getByTestId('create-task');
        expect(textInput).toBeTruthy();
        fireEvent(textInput,'onChangeText','newtask');
        fireEvent.press(createTask);    
        expect(()=>getByText('newtask')).toBeTruthy();
    })
    it("should add multiple task",()=>{
        const firstTask = 'task1';
        const secondTask = 'task2';
        const  {debug,getByA11yLabel,getByTestId,getByPlaceholderText,getByText} = render(<App/>)
        const addButton = getByTestId('add-button');
        expect(addButton).toBeTruthy();
        fireEvent.press(addButton);
        let textInput = getByPlaceholderText('Enter Task');
        let createTask = getByTestId('create-task');
        expect(textInput).toBeTruthy();
        fireEvent.changeText(textInput, firstTask);
        // fireEvent(textInput,'onChangeText','newtask');
        fireEvent.press(createTask);    
        fireEvent.press(addButton);
        textInput = getByPlaceholderText('Enter Task');
        createTask = getByTestId('create-task');
        fireEvent.changeText(textInput, secondTask);        
        fireEvent.press(createTask);    
        expect(()=>getByText(firstTask)).toBeTruthy();        
        expect(()=>getByText(secondTask)).toBeTruthy();        
    })
    it("should delete task",()=>{
        const  {debug,getByA11yLabel,getByTestId,getByPlaceholderText,getByText} = render(<App/>)
        const addButton = getByTestId('add-button');
        expect(addButton).toBeTruthy();
        fireEvent.press(addButton);
        const textInput = getByPlaceholderText('Enter Task');
        const createTask = getByTestId('create-task');
        expect(textInput).toBeTruthy();
        fireEvent(textInput,'onChangeText','newtask');
        fireEvent.press(createTask);    
        expect(()=>getByText('newtask')).toBeTruthy();
        const deleteButton = getByTestId('delete');
        expect(deleteButton).toBeTruthy();
        fireEvent.press(deleteButton);        
        expect(()=>getByText('newtask')).toThrowError();        
    })
    it("should add a Task and change date",()=>{
        const  {getByTestId,getByPlaceholderText,getByText} = render(<App/>)
        const addButton = getByTestId('add-button');
        expect(addButton).toBeTruthy();
        fireEvent.press(addButton);
        const textInput = getByPlaceholderText('Enter Task');
        const createTask = getByTestId('create-task');
        expect(textInput).toBeTruthy();
        fireEvent(textInput,'onChangeText','newtask');
        fireEvent.press(createTask);    
        expect(()=>getByText('newtask')).toBeTruthy();
    })
})
