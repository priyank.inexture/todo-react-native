/* eslint-disable no-nested-ternary */
import React, { createContext, useReducer, useContext } from "react";
// eslint-disable-next-line max-len
import {
  UPATE_TASKS_ACTION,
  UPDATE_STATUS_ACTION,
  DELETE_TASK_ACTION,
} from "./ActionType";
import { taskList } from "../utils/index";
//Define Conext
const GlobalStateContext = createContext();
const GlobalDispatchContext = createContext();
// const storeData = async (value) => {
//   try {
//     await AsyncStorage.setItem("tasks", value);
//   } catch (e) {
//     // saving error
//   }
// };
// const getData = async () => {
//   try {
//     const value = await AsyncStorage.getItem("tasks");
//     if (value !== null) {
//       return value;
//     }
//   } catch (e) {
//     return [];
//   }
// };
//Reducer
const globalReducer = (state, action) => {
  switch (action.type) {
    case UPATE_TASKS_ACTION: {
      return {
        ...state,
        tasks: action.tasks,
      };
      // storeData(newTasks);
    }
    case UPDATE_STATUS_ACTION: {
      let task = state.tasks.find((t) => t.id == action.id);
      if (task) task.status = action.status;
      const newTasks = [
        ...state.tasks.map((t) => (t.id === action.id ? task : t)),
      ];
      // storeData(newTasks);
      return {
        ...state,
        tasks: newTasks,
      };
    }
    case DELETE_TASK_ACTION: {
      const newTasks = [
        ...state.tasks
          .map((t) => (t.id === action.id ? null : t))
          .filter((x) => x),
      ];
      // storeData(newTasks);
      return {
        ...state,
        tasks: newTasks,
      };
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

//Provider

// eslint-disable-next-line react/prop-types
export const GlobalProvider = ({ children }) => {
  // let data = getData();
  // console.log("saw data", data);
  const [state, dispatch] = useReducer(globalReducer, {
    // tasks: taskList,
    tasks: [],
  });

  return (
    <GlobalDispatchContext.Provider value={dispatch}>
      <GlobalStateContext.Provider value={state}>
        {children}
      </GlobalStateContext.Provider>
    </GlobalDispatchContext.Provider>
  );
};

//custom hooks for when we want to use our global state
export const useGlobalStateContext = () => useContext(GlobalStateContext);

export const useGlobalDispatchContext = () => useContext(GlobalDispatchContext);
