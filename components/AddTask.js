import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  Button,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import { TextInput } from "react-native-gesture-handler";
import {
  useGlobalDispatchContext,
  useGlobalStateContext,
} from "../context/globalContext";
import { UPATE_TASKS_ACTION } from "../context/ActionType";
function AddTask({ navigation }) {
  const dispatch = useGlobalDispatchContext();
  const { tasks } = useGlobalStateContext();
  const today = new Date();
  const [date, setDate] = useState(today);
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);
  const [finalDate, setFinalDate] = useState(today);
  const [taskText, setTaskText] = useState("");
  const [taskDate, setTaskDate] = useState([
    today.getFullYear(),
    today.getMonth(),
    today.getDate(),
  ]);
  const [taskTime, setTaskTime] = useState([
    today.getHours(),
    today.getMinutes(),
  ]);
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);
    if (mode === "date") {
      setTaskDate([
        currentDate.getFullYear(),
        currentDate.getMonth(),
        currentDate.getDate(),
      ]);
      //   setMode("time");
    } else {
      setTaskTime([currentDate.getHours(), currentDate.getMinutes()]);
      //   setShow(false);
    }
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };
  const handleCreateTask = () => {
    const newTask = {
      id: Math.random().toString(),
      text: taskText,
      time: finalDate,
      status: false,
    };
    const newTasks = [...tasks, newTask];
    dispatch({ type: UPATE_TASKS_ACTION, tasks: newTasks });
    console.log(newTasks);
    navigation.navigate("Home");
  };
  useEffect(() => {
    // console.log(taskDate, taskTime);
    setFinalDate(new Date(...taskDate, ...taskTime));
  }, [taskDate, taskTime]);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.mainContainer}>
        <View>
          <View>
            <TextInput
              placeholder="Enter Task"
              style={styles.textInput}
              onChangeText={(val) => setTaskText(val)}
            />
          </View>
          {show && (
            <DateTimePicker
              value={date}
              mode={mode}
              is24Hour={false}
              display="default"
              onChange={onChange}
            />
          )}
          <View>
            <Text style={styles.time}>Time : {finalDate.toLocaleString()}</Text>
            <View>
              <Button onPress={showDatepicker} title="Edit date" />
            </View>
            <View style={styles.bottomBtn}>
              <Button onPress={showTimepicker} title="Edit time" />
            </View>
          </View>
        </View>
        <View>
          <Button title="Create Task" onPress={handleCreateTask} testID="create-task"/>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
const styles = StyleSheet.create({
  textInput: {
    margin: 15,
    height: 50,
    borderBottomWidth: 1,
      borderBottomColor: "#ccc",
    },
    bottomBtn: {
      marginTop: 15,
    },
    time: {
      margin: 15,
  },
  mainContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    paddingBottom: 15,
  },
});
export default AddTask;
