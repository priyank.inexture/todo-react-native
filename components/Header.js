import React from "react";
import { View, Text, StyleSheet } from "react-native";

export default function Header(props) {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>
        {props.title ? props.title : "Demo-app"}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    height: 65,
    paddingTop: 18,
  },
  headerText: {
    fontSize: 21,
    textAlign: "center",
    fontWeight: "bold",
    color: "black",
  },
});
