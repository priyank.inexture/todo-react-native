import React from "react";
import { Text, StyleSheet, View } from "react-native";
import CheckBox from '@react-native-community/checkbox';
import { MaterialIcons } from "@expo/vector-icons";
import { useGlobalDispatchContext } from "../context/globalContext";
import {
  UPDATE_STATUS_ACTION,
  DELETE_TASK_ACTION,
} from "../context/ActionType";
import { TouchableOpacity } from "react-native-gesture-handler";
function Task({ id, text, status }) {
  const dispatch = useGlobalDispatchContext();
  const hadleStatusChange = () => {
    // console.log("completed", id);
    dispatch({ type: UPDATE_STATUS_ACTION, status: !status, id: id });
  };
  const handleDelete = () => {
    dispatch({ type: DELETE_TASK_ACTION, id: id });
  };
  return (
    <View style={styles.taskContainer}>
      <View style={styles.taskItem}>
        {/* <TouchableOpacity onPress={hadleStatusChange} >           */}
            <CheckBox value={status}  onValueChange={hadleStatusChange} testID="status"/>
        {/* </TouchableOpacity> */}
        <Text style={styles.text} accessibilityLabel="task">{text}</Text>
      </View>
      <View style={styles.deleteItem} >
        <MaterialIcons
          name="delete"
          size={21}
          color="#999"        
          onPress={handleDelete} testID="delete"
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  taskContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: "#bbb",
    borderRadius: 25,
    padding: 5,
    margin: 5,
  },
  taskItem: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  deleteItem: {
    marginTop: 5,
    marginRight: 5,
  },
  text: {
    fontSize: 16,
    marginLeft: 5,
  },
});
export default Task;
