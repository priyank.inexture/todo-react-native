import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TextInput,
  Button,
  SectionList,
} from "react-native";
import EmptyTask from "./EmptyTask";
import { getDateType } from "../utils/index";
import {
  useGlobalStateContext,
  useGlobalDispatchContext,
} from "../context/globalContext";
import Task from "./Task";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { UPATE_TASKS_ACTION } from "../context/ActionType";
function NewTodo({ navigation }) {
  const [DATA, setDATA] = useState([]);
  const { tasks } = useGlobalStateContext();
  const dispatch = useGlobalDispatchContext();
  const storeData = async (value) => {
    try {
      await AsyncStorage.setItem("tasks", JSON.stringify(value));
    } catch (e) {
      // saving error
    }
  };
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem("tasks");
      if (value !== null) {
        return value;
      }
    } catch (e) {
      // error reading value
    }
    return [];
  };
  useEffect(() => {
    getData().then((res) => {
      // console.log("found", res);
      let oldTask = JSON.parse(res);
      const newTasks =
        oldTask.length > 0
          ? oldTask.map((t) => ({ ...t, time: new Date(t.time) }))
          : [];
      if (res.length > 0)
        dispatch({ type: UPATE_TASKS_ACTION, tasks: newTasks });
    });
  }, []);
  useEffect(() => {
    let today = [];
    let tomorrow = [];
    let upcoming = [];
    tasks.forEach((t) => {
      switch (getDateType(t.time)) {
        case 0:
          today.push(t);
          break;
        case 1:
          tomorrow.push(t);
          break;
        case 2:
          upcoming.push(t);
          break;
        default:
          break;
      }
    });
    today.sort((a, b) => a.time.getTime() - b.time.getTime());
    tomorrow.sort((a, b) => a.time.getTime() - b.time.getTime());
    upcoming.sort((a, b) => a.time.getTime() - b.time.getTime());
    // setTodaysTask(today);
    // setTomorrowsTask(tomorrow);
    // setUpcommingTask(upcoming);
    setDATA([
      {
        title: "Today",
        data: today,
      },
      {
        title: "Tomorrow",
        data: tomorrow,
      },
      {
        title: "Upcomming",
        data: upcoming,
      },
    ]);
    // console.log("setting", tasks);
    storeData(tasks);
  }, [tasks]);
  return (
    <View testID="todo-container">
      {DATA.reduce(
        (total, value, index, array) => total + value.data.length,
        0
      ) === 0 && <EmptyTask />}
      {DATA.reduce(
        (total, value, index, array) => total + value.data.length,
        0
      ) > 0 && (
        <SectionList
          sections={DATA}
          keyExtractor={(item, index) => item.id + index}
          renderItem={({ item }) => <Task {...item} />}
          renderSectionHeader={({ section: { title, data } }) =>
            data.length > 0 && <Text style={styles.header}>{title}</Text>
          }
        />
      )}
      <View style={styles.button}>
        <Button title="add" onPress={() => navigation.navigate("Add")} testID="add-button"/>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  header: {
    fontWeight: "bold",
    fontSize: 21,
    marginTop: 5,
    marginBottom: 5,
  },
  button: {
    marginBottom: 12,
  },
});
export default NewTodo;
