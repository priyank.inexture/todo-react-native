import React from "react";
import { Text, View, StyleSheet } from "react-native";

function EmptyTask() {
  return <Text style={styles.text}>No Task</Text>;
}
const styles = StyleSheet.create({
  text: {
    fontSize: 24,
    color: "#777",
    margin: 15,
  },
});
export default EmptyTask;
