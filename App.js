import React, { useEffect } from "react";
import { SafeAreaView, StatusBar, StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {
  GlobalProvider,
  useGlobalDispatchContext,
} from "./context/globalContext";
import Todo from "./components/Todo";
import NewTodo from "./components/NewTodo";
import AddTask from "./components/AddTask";
const Stack = createStackNavigator();
export default function App() {
  return (    
      <GlobalProvider>        
        <SafeAreaView style={styles.container} testID="container">
        <NavigationContainer>
          <Stack.Navigator>
            {/* <Stack.Screen
              name="Home"
              options={{ title: "All Tasks" }}
              component={Todo}
            /> */}
            <Stack.Screen
              name="Home"
              options={{ title: "All Tasks" }}
              component={NewTodo}
            />
            <Stack.Screen
              name="Add"
              options={{ title: "Create Task" }}
              component={AddTask}
            />
          </Stack.Navigator>
          </NavigationContainer>
        </SafeAreaView>
      </GlobalProvider>    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    paddingHorizontal: 10,
    backgroundColor: "#f5f8ff",
  },
});
